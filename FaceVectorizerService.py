import cv2
import os

from constants import WORK_DIR
from face_vectorizer import OpenvinoFaceVectorizer
from openvino_detectors.FaceDetector import FaceDetector
import numpy as np

# img = cv2.imread(os.path.join(WORK_DIR, "assets", "water2.jpg"))
# (h, w) = img.shape[:2]
# img = cv2.resize(img, (int(w/2), int(h/2)))
# cv2.imwrite(os.path.join(WORK_DIR, "assets", "water3.jpg"), img)

class FaceVectorizerService:
    def __init__(self):
        self.face_detector = FaceDetector()
        self.vectorizer = OpenvinoFaceVectorizer()

        self.watermark_path = os.path.join(WORK_DIR, "assets", "water_17_06_2020.jpeg")
        self.watermark = self.prepare_watermark(self.watermark_path)

        self.corner_watermark_path = os.path.join(WORK_DIR, "assets", "water_20_06_2020.jpeg")
        self.corner_watermark = self.prepare_watermark(self.corner_watermark_path)



    def prepare_watermark(self, path):
        watermark = cv2.imread(path)
        watermark = cv2.cvtColor(watermark, cv2.COLOR_RGB2RGBA)
        (B, G, R, A) = cv2.split(watermark)
        B = cv2.bitwise_and(B, B, mask=A)
        G = cv2.bitwise_and(G, G, mask=A)
        R = cv2.bitwise_and(R, R, mask=A)
        watermark = cv2.merge([B, G, R, A])

        return watermark

    def load_images(self, path_to_images):
        self.vectorizer.clear_db()
        file_system_files = os.walk(path_to_images)
        for root, dirs, files in file_system_files:
            for file in files:
                self.add_new_image(os.path.join(root, file), "")

        print("Database created")
        print("Db size =", len(self.vectorizer.vectors))

    def add_new_image(self, img_path, target_img_path):
        """
        Если мы передали пустую строку целевого пути, то считаем что картинку проверять не нужно
        и картинка уже находиться у нас в базе,
        поэтому мы просто записываем ее в базу.

        :param img_path:
        :param target_img_path:
        :return:
        """
        face = cv2.imread(img_path, cv2.IMREAD_COLOR)
        if face is None:
            return False
        detections = self.face_detector.detect(face)
        if len(detections) == 0:
            return False
        cur_faces = [
            self.face_detector.crop(face, det) for det in detections
        ]
        # Если не передан путь для целевой картинки то используем в качестве идентификатора
        # путь переданной картинки
        target_img_path = target_img_path if target_img_path else img_path
        self.vectorizer.add_faces(cur_faces, target_img_path)
        return True

    def find_by_img(self, img_path, threshold=0.4, top=20):
        face = cv2.imread(img_path, cv2.IMREAD_COLOR)
        detections = self.face_detector.detect(face)
        if len(detections) == 0:
            return "Face not found"
        if len(detections) > 1:
            return "Many faces found"

        cur_face = self.face_detector.crop(face, detections[0])
        cur_result = self.vectorizer.searcher(cur_face, top=top)
        result = []
        for similarity, id_people in cur_result:
            if similarity > threshold:
                result.append((similarity, id_people))

        return result

    def add_blur(self, img_path, target_img_path):
        image = cv2.imread(img_path)
        detections = self.face_detector.detect(image)
        faces = []
        for det in detections:
            cur_face = self.face_detector.crop(image, det)
            faces.append(cur_face)

        res = cv2.GaussianBlur(image, (101, 101), 0)

        for i, face in enumerate(faces):
            detection = detections[i]
            res[detection[1]:detection[3], detection[0]:detection[2]] = face
        cv2.resize(res, (200, 200))
        cv2.imwrite(target_img_path, res)

    def add_corner_watermark(self, img_path, target_img_path):

        # watermark= cv2.cvtColor(watermark, cv2.COLOR_RGB2RGBA)
        (wH, wW) = self.corner_watermark.shape[:2]
        # (B, G, R, A) = cv2.split(watermark)
        # B = cv2.bitwise_and(B, B, mask=A)
        # G = cv2.bitwise_and(G, G, mask=A)
        # R = cv2.bitwise_and(R, R, mask=A)
        # watermark = cv2.merge([B, G, R, A])

        image = cv2.imread(img_path)
        (h, w) = image.shape[:2]
        image = np.dstack([image, np.ones((h, w), dtype="uint8") * 255])

        # construct an overlay that is the same size as the input
        # image, (using an extra dimension for the alpha transparency),
        # then add the watermark to the overlay in the bottom-right
        # corner
        overlay = np.zeros((h, w, 4), dtype="uint8")
        overlay[h - wH - 10:h - 10, w - wW - 10:w - 10] = self.corner_watermark

        # blend the two images together using transparent overlays
        output = image.copy()
        cv2.addWeighted(overlay, 1, output, 1.0, 0, output)
        cv2.imwrite(target_img_path, output)

    def add_watermark(self, img_path, output_path):

        img = cv2.imread(img_path)
        watermark = cv2.imread(self.watermark_path)

        width = 250
        # height = auto
        c = img.shape[1] / width
        height = int(img.shape[0] / c)

        img_resized = cv2.resize(img, (width, height))

        (h, w) = img_resized.shape[:2]
        image = np.dstack([img_resized, np.ones((h, w), dtype="uint8") * 255])

        mid_h = int(h / 2)
        mid_w = int(w / 2)
        # construct an overlay that is the same size as the input
        # image, (using an extra dimension for the alpha transparency),
        # then add the watermark to the overlay in the bottom-right
        # corner
        (wH, wW) = watermark.shape[:2]
        mid_wH = int(wH / 2)
        mid_wW = int(wW / 2)
        watermark = cv2.cvtColor(watermark, cv2.COLOR_BGR2BGRA)
        (B, G, R, A) = cv2.split(watermark)
        B = cv2.bitwise_and(B, B, mask=A)
        G = cv2.bitwise_and(G, G, mask=A)
        R = cv2.bitwise_and(R, R, mask=A)
        watermark = cv2.merge([B, G, R, A])

        overlay = np.zeros((h, w, 4), dtype="uint8")
        overlay[mid_h - mid_wH:mid_h + mid_wH, mid_w - mid_wW:mid_w + mid_wW] = watermark
        # blend the two images together using transparent overlays
        output = image.copy()
        cv2.addWeighted(overlay, 0.4, output, 1.0, 0, output)
        cv2.imwrite(output_path, output)
        # return output

    def add_watermark_mozaik(self, img_path, target_img_path):

        # watermark= cv2.cvtColor(watermark, cv2.COLOR_RGB2RGBA)
        (wH, wW) = self.watermark.shape[:2]
        # (B, G, R, A) = cv2.split(watermark)
        # B = cv2.bitwise_and(B, B, mask=A)
        # G = cv2.bitwise_and(G, G, mask=A)
        # R = cv2.bitwise_and(R, R, mask=A)
        # watermark = cv2.merge([B, G, R, A])

        image = cv2.imread(img_path)
        (h, w) = image.shape[:2]
        newWatermark = cv2.resize(self.watermark, (w, h))
        image = np.dstack([image, np.ones((h, w), dtype="uint8") * 255])

        # construct an overlay that is the same size as the input
        # image, (using an extra dimension for the alpha transparency),
        # then add the watermark to the overlay in the bottom-right
        # corner
        overlay = np.zeros((h, w, 4), dtype="uint8")
        overlay[0:h, 0:w] = newWatermark

        # blend the two images together using transparent overlays
        output = image.copy()
        cv2.addWeighted(overlay, 0.25, output, 1.0, 0, output)
        output = cv2.resize(output, (int(w/4), int(h/4)))
        cv2.imwrite(target_img_path, output)


# if __name__ == "__main__":
#     service = FaceVectorizerService()
#     service.load_images("/home/sergej2/data_park_2")
#
#     res1 = service.find_by_img(
#         "/home/sergej2/test_faces/1.jpg")
#     print(res1)
#
#     res1 = service.find_by_img(
#         "/home/sergej2/test_faces/2.jpg")
#     print(res1)
#     res1 = service.find_by_img(
#         "/home/sergej2/test_faces/3.jpg")
#     print(res1)


if __name__ == "__main__":
    service = FaceVectorizerService()
    service.add_blur("/home/sergej2/data_park_2/data_park/IMG_4490.jpg",
                     "/home/sergej2/PycharmProjects/face/media/test_blur.jpg")
    # service.add_watermark("/home/sergej2/PycharmProjects/face/media/raw.jpg","/home/sergej2/PycharmProjects/face/media/test.jpg")
