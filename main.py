"""
Главная точка входа в приложение.
"""
import argparse
import asyncio
import hashlib
import logging
import os
import random
import re
import shutil
import tempfile
import urllib
import socket
from datetime import datetime
from pathlib import Path
from uuid import uuid4


import aiohttp
import aiohttp_cors
import aiohttp_jinja2
import jinja2
import uvloop
from aiofile import AIOFile, Writer
from aiohttp import web, FormData
from aiohttp_swagger import setup_swagger

import FaceVectorizerService
from constants import WORK_DIR
from jwt_token_manager import jwt_middleware_factory

MEDIA_DIR = Path(WORK_DIR, 'media')
TEMPLATE_PATH = Path(WORK_DIR, 'templates')

DOWNLOADING_IMAGES_PATH = '/download/media'
STORED_IMAGE_FOLDER_NAME = Path(MEDIA_DIR)
STORED_IMAGE_FOLDER_NAME.mkdir(exist_ok=True)

HTTP_SCHEME = os.environ.get("HTTP_SCHEME", "https")

get_host_name = socket.gethostbyname(socket.gethostname())

face_vectorizer_service = FaceVectorizerService.FaceVectorizerService()

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger('main')

@aiohttp_jinja2.template('index.html')
async def idexpage(request):
    """
    description: This end-point perform hard calculating actions.
    tags:
    - ControlPage
    produces:
    - text/plain
    responses:
        "200":
            description: html control page
    """
    return {}


async def get_vectors(request):
    """
    ---
    description: Returns 20 vectors
    tags:
    - Get Vectors
    produces:
    - application/json
    responses:
        "200":
            description: vectors array.
    """
    vectors = list(face_vectorizer_service.vectorizer.vectors.keys())
    return web.json_response({
        "result": vectors
    })


def check_image_request(content_type):
    """
    Проверить тип загруженного файла.
    :return:
    """
    valid_content_types = {
        "image/jpeg": "jpeg",
        "image/jpg": "jpg",
        "image/bmp": "bmp",
        "image/png": "png"
    }
    try:
        img_extension = valid_content_types[content_type]
    except KeyError:
        raise aiohttp.web.HTTPBadRequest(
            reason='Provide valid content type: {}'.format(list(valid_content_types.keys()))
        )
    return img_extension


def check_name_space(user_payload):
    """
    Проверка области сайта.
    :return:
    """
    name_space = user_payload.get('name_space')
    if name_space is None:
        raise aiohttp.web.HTTPBadRequest(
            reason='Name space not found please provide name space'
        )
    return name_space


def create_folders(folders_list):
    
    for folder in folders_list:
        folder.mkdir(exist_ok=True, parents=True)

def remove_folders(file_path):
    try:
        os.remove(file_path)
    except:
        shutil.rmtree(file_path)


def process_image(image_path, file_folder, target_img_path):
    """
    Предача изображения нейронке.

    :param file_folder:
    :param target_img_path:
    :param image_path:
    :return:
    """

    faces_finded = face_vectorizer_service.add_new_image(image_path, target_img_path)
    if not faces_finded:
        return "Faces not found"

    file_folder.mkdir(exist_ok=True, parents=True)
    # shutil.copy(image_path, target_img_path)
    try:
        shutil.copy(image_path, target_img_path)
        #face_vectorizer_service.add_corner_watermark(image_path, target_img_path)
    except ValueError:
        return "Bad image size"
    return None


#creating main folder while created access tokens
async def add_site_folders(request):

    current_loop = asyncio.get_running_loop()

    name_space = check_name_space(request['user_payload'])

    file_folder = Path(STORED_IMAGE_FOLDER_NAME, name_space,'storedimages', datetime.now().strftime("%Y%m%d"))
    blur_folder = Path(STORED_IMAGE_FOLDER_NAME, name_space, 'blur', datetime.now().strftime("%Y%m%d"))
    temp_folder = Path(STORED_IMAGE_FOLDER_NAME, name_space, 'temp')

    folders_list = [file_folder, blur_folder, temp_folder]

    await current_loop.run_in_executor(
        None,
        create_folders,
        folders_list,
    )

    return web.json_response({})

def check_date_folders(folders_list, parent_folder, name_space):
    today = int(datetime.now().strftime("%Y%m%d"))
    delete_list = []
    
    for folder in folders_list:
        folder = int(folder)
        if (today - folder)>=6:
            folder = Path(STORED_IMAGE_FOLDER_NAME, name_space, parent_folder, str(folder))
            delete_list.append(folder)
    return delete_list

async def delete_old_images(request):

    current_loop = asyncio.get_running_loop()
    name_space = check_name_space(request['user_payload'])

    storedimages = Path(STORED_IMAGE_FOLDER_NAME, name_space,'storedimages')
    blur = Path(STORED_IMAGE_FOLDER_NAME, name_space, 'blur')

    storedimages_folders = os.listdir(storedimages)
    blur_folders = os.listdir(blur)

    delete_storage_folders_list = await current_loop.run_in_executor(None, check_date_folders, storedimages_folders, 'storedimages', name_space)
    delete_blur_folders_list = await current_loop.run_in_executor(None, check_date_folders, blur_folders, 'blur', name_space)

    for folder in delete_storage_folders_list:
        await current_loop.run_in_executor(None, remove_folders, str(folder))

    for folder in delete_blur_folders_list:
        await current_loop.run_in_executor(None, remove_folders, str(folder))
    
    return web.json_response({})

async def upload_image(request):
    """
    ---
    description: Load a new photo.
    tags:
    - PhotoUploading
    produces:
    - application/json
    requestBody:
      content:
        image/jpeg:
          schema:
            type: string
            format: binary
    responses:
        "200":
            description: a successful image uploading.
    """

    img_extension = check_image_request(request.content_type)
    utctimestamp = int(datetime.utcnow().timestamp())
    received_file_name = "{}_{}.{}".format(
        utctimestamp, str(uuid4()), img_extension
    )

    name_space = check_name_space(request['user_payload'])
    file_folder = Path(STORED_IMAGE_FOLDER_NAME, name_space,'storedimages', datetime.now().strftime("%Y%m%d"))
    folders_list = [file_folder]

    current_loop = asyncio.get_event_loop()

    await current_loop.run_in_executor(None, create_folders, folders_list)

    target_image_path = os.path.join(file_folder, received_file_name)
    with tempfile.TemporaryDirectory() as tmp_files_dir:
        size = 0
        tmp_image_path = os.path.join(tmp_files_dir, received_file_name)
        async with AIOFile(tmp_image_path, 'wb') as afp:
            writer = Writer(afp)
            while True:
                chunk, _ = await request.content.readchunk()
                chunk_length = len(chunk)  # 8192 bytes by default.
                if not chunk:
                    break
                size += chunk_length
                await writer(chunk)
            await afp.fsync()
        faces_finded = await current_loop.run_in_executor(
            None,
            process_image,
            tmp_image_path,
            file_folder,
            target_image_path
        )
    if faces_finded is not None:
        raise web.HTTPBadRequest(reason="Image processing result: {}".format(faces_finded))
    return web.json_response({
        "result": '{} sized of {} successfully stored'.format(received_file_name, size)
    })

#upload images from temp to storedimages
async def give_temp_images(request):
    current_loop = asyncio.get_running_loop()
    
    name_space = check_name_space(request['user_payload'])
    temp_folder = Path(STORED_IMAGE_FOLDER_NAME, name_space, 'temp')
    file_folder = Path(STORED_IMAGE_FOLDER_NAME, name_space,'storedimages', datetime.now().strftime("%Y%m%d"))

    try:
        images_list = os.listdir(temp_folder)
        count_images = len(images_list)


        for i in range(0, count_images):
                image = images_list[i]
                tmp_image_path = str(temp_folder) + '/' + str(image)
                target_image_path = os.path.join(file_folder, str(image))

                await current_loop.run_in_executor(
                    None,
                    process_image,
                    tmp_image_path,
                    file_folder,
                    target_image_path
                )

                await current_loop.run_in_executor(None, remove_folders, tmp_image_path)

        return web.json_response({})
    except:
        return web.json_response(text='No temp folder')


def find_img(requested_image_path):
    """
    Поиск похожих картинок.

    Выполнение поиска.
    :param image_path:
    :return:
    """
    similar_images_paths = face_vectorizer_service.find_by_img(requested_image_path)
    if type(similar_images_paths) is not list:
        raise web.HTTPBadRequest(reason=f"Find image result: {similar_images_paths}")
    similar_images_paths_blured = []

    for similarity, find_img_path in similar_images_paths:
        stored_img_path_name = Path(find_img_path).name

        parent_folder_name = Path(find_img_path).parts[len(STORED_IMAGE_FOLDER_NAME.parts):-1]

        blured_image_folder = Path(MEDIA_DIR, parent_folder_name[0], 'blur', parent_folder_name[2])
 
        blured_image_folder.mkdir(exist_ok=True, parents=True)
        blured_image_name = Path(blured_image_folder, stored_img_path_name)
        similar_images_paths_blured.append((
            similarity, blured_image_name
        ))
        if blured_image_name.exists():
            continue
        # face_vectorizer_service.add_blur(find_img_path, str(blured_image_name))
        face_vectorizer_service.add_watermark(find_img_path, str(blured_image_name))

    return similar_images_paths_blured


async def find_images_and_blur(request):
    """
    ---
    description: Find similar photos by img.
    tags:
    - PhotoSearching
    produces:
    - application/json
    requestBody:
      content:
        image/jpeg:
          schema:
            type: string
            format: binary
    responses:
        "200":
            description: similar images links.
    """
    # print(request)
    _ = check_image_request(request.content_type)
    name_space = check_name_space(request['user_payload'])
    with tempfile.TemporaryDirectory() as tmp_files_dir:
        received_file_name = f"{str(uuid4())}"
        provided_image_path = os.path.join(tmp_files_dir, received_file_name)
        async with AIOFile(provided_image_path, 'wb') as afp:
            writer = Writer(afp)
            while True:
                chunk, _ = await request.content.readchunk()
                if not chunk:
                    break
                await writer(chunk)
            await afp.fsync()
        current_loop = asyncio.get_running_loop()
        similar_images_paths = await current_loop.run_in_executor(None, find_img, provided_image_path)
    LOGGER.info('Finding results {}'.format(similar_images_paths))
    print('Finding results {}'.format(similar_images_paths))
    # TODO Разобраться с прокидыванием схемы
    base_url = f"{HTTP_SCHEME}://{request.host}"
    download_images_links = []
    for similarity_distance, image_path in similar_images_paths:
        img_path_to_url = urllib.parse.quote(
            str(image_path).replace(str(MEDIA_DIR), '', 1)
        )
        try:
            if name_space == image_path.parts[len(STORED_IMAGE_FOLDER_NAME.parts)]:
                unix_time = int(image_path.name.split('_')[0])
                download_images_links.append({
                    "img_url": f"{base_url}{DOWNLOADING_IMAGES_PATH}{img_path_to_url}",
                    "similarity_distance": similarity_distance,
                    "utc_timestamp": unix_time,
                    "utc_iso_time": datetime.utcfromtimestamp(unix_time).isoformat()
                })
        except (IndexError, ValueError):
            # Если эти ошибки значит это старый или не верный формат фоток
            print('Old image format path = ', str(image_path))
    if not download_images_links:
        raise web.HTTPNotFound(reason="Similar not found")
    return web.json_response(download_images_links)


async def download_image(request):
    """
    ---
    description: Download photo.
    tags:
    - PhotoDownloading
    produces:
    - image
    responses:
        "200":
            description: image file.
    """
    try:
        image_path_from_request = request.path.replace(DOWNLOADING_IMAGES_PATH, "", 1).split("/")[1:]
        _ = image_path_from_request[0]
    except IndexError:
        raise aiohttp.web.HTTPBadRequest(reason='No found image path')
    return web.FileResponse(
        Path(MEDIA_DIR, *image_path_from_request)
    )


FREE_END_POINTS = [
    DOWNLOADING_IMAGES_PATH +
    '/.+/blur/.*[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}\\..{2,6}'
]

app = web.Application(
    middlewares=[jwt_middleware_factory(FREE_END_POINTS)]
)
aiohttp_jinja2.setup(
    app, loader=jinja2.FileSystemLoader(str(TEMPLATE_PATH))
)

cors = aiohttp_cors.setup(app, defaults={
    "*": aiohttp_cors.ResourceOptions(
        allow_credentials=True,
        expose_headers="*",
        allow_headers="*",
        allow_methods=["GET"]
    )
})

app.add_routes([web.static(f'{DOWNLOADING_IMAGES_PATH}', str(MEDIA_DIR), show_index=False)])
for route in list(app.router.routes()):
    cors.add(route)

app.add_routes([web.get('/api/vectors', get_vectors)])

upload_resource = cors.add(app.router.add_resource('/api/upload/image'))
cors.add(
    upload_resource.add_route("POST", upload_image),
    {
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*"
        ),
    }
)

upload_resource = cors.add(app.router.add_resource('/api/find/similar'))
cors.add(
    upload_resource.add_route("POST", find_images_and_blur),
    {
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        ),
    }
)


setup_swagger(app, swagger_url='swagger')
app.add_routes([web.get('/createsite', add_site_folders)])
app.add_routes([web.get('/give_temp_images', give_temp_images)])
app.add_routes([web.get('/delete_old_images', delete_old_images)])
app.add_routes([web.static(f'/mediaexplorer', str(MEDIA_DIR), show_index=True)])
app.add_routes([web.get('/demo', idexpage)])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--create_db_mode', type=bool, default=False, help='foo help')
    args = parser.parse_args()
    if args.create_db_mode:
        face_vectorizer_service.load_images(STORED_IMAGE_FOLDER_NAME)
        exit(0)
    uvloop.install()
    web.run_app(app)