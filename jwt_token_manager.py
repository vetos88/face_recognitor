"""
Менеджер работы с JWT токенами.
"""
import logging
import os
import re

import jwt
from aiohttp import web
from aiohttp.web_middlewares import middleware
from jwt import InvalidTokenError, ExpiredSignatureError

PYJWT_PUBLIC_KEY = os.path.join('/jwtkeys', os.environ.get('PYJWT_PUBLIC_KEY', 'jwtRS256.key.pub'))

logger = logging.getLogger('jst_producer')


class TokenProducer:
    def __init__(self):
        """
        Считывание ключей
        """
        with open(PYJWT_PUBLIC_KEY, 'rb') as public_key_file:
            self.public_key = public_key_file.read()

    def decode_key(self, jwt_token):
        """
        Расшифровка токена.
        :return:
        """
        decoded = jwt.decode(jwt_token, self.public_key, algorithms='RS256')
        return decoded


def jwt_middleware_factory(exception_and_points=None):
    jwt_token_producer = TokenProducer()

    def checkin_jwt(request):
        """
        Метод определения нужна ли проверка jwt при запросе.

        По умолчанию проверяются все end-pointы и все методы кроме
        OPTIONS и HEAD.
        Чтобы отключить проверку для конкретного end pointа на чтение(метод GET)
        можно передать его в массиве исключений в виде регулярного выражения.
        :param request:
        :return:
        """
        requested_url = str(request.rel_url)
        if request.method == 'GET' and exception_and_points is not None:
            for exception_and_point in exception_and_points:
                pattern = re.compile(exception_and_point)
                exception_case = pattern.match(requested_url)
                if exception_case is not None:
                    return False
            return True
        if request.method in ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']:
            return True

    @middleware
    async def jwt_middleware(request, handler):

        if checkin_jwt(request):
            try:
                jwt_token = request.headers['Authorization']
            except KeyError:
                raise web.HTTPForbidden(reason='jwt not provided')
            try:
                jwt_token = jwt_token.split(' ')[1]
            except IndexError:
                raise web.HTTPForbidden(reason='jwt bad format. provide Bearer token')
            try:
                user_payload = jwt_token_producer.decode_key(jwt_token)
                if user_payload.get('type') != 'ACCESS':
                    raise InvalidTokenError('Invalid token type')
                request['user_payload'] = user_payload
            except ExpiredSignatureError:
                raise web.HTTPForbidden(reason='jwt expired')
            except InvalidTokenError as err:
                logging.warning('Provided bad token %s err %s', jwt_token, repr(err))
                raise web.HTTPForbidden(reason='jwt not valid')
        resp = await handler(request)
        return resp

    return jwt_middleware
