#!/usr/bin/env bash

sudo apt-get install apache2-utils
htpasswd -c ~/projects/face_rec/face_recognitor/traefik_config/usersfile vetos

export $(cat .run-env)

docker-compose up -d --scale neiro-http=4
docker-compose down

docker logs -f face_recognitor_reverse-proxy_1

docker-compose -f traefik-compose.yml up -d
docker-compose -f traefik-compose.yml down

docker pull vetos/neirofacerec:http-serv
git pull