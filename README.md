# FaceRecognitor
### Http сервис по распознаванию лиц.

Для сборки образа нужно скачать библиотеку openvino.

Сборка состоит из двух частей.

## Сборка базового образа с библиотекой openvino и python3.7
```bash
docker build . -t neirofacerec:all-env -f DockerfileOpenVinoEnv
```

## Сборка образа с зависимостями сервера.
```bash
docker build . -t neirofacerec:http-serv -f DockerfileHttpInterface
```


## Запуск на 4 процесса
```bash
docker-compose up --scale web=4
```

```bash
docker run --name local-redis-celery \
    --network="local-apps" \
    --restart always \
    -v ~/Documents/storedata/redisdata:/data \
    -p 6382:6379 \
    -d redis redis-server --appendonly yes
```

'openvino_lib/libcpu_extension_sse4.so'
