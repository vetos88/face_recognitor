import cv2
import time
def recognize(path_to_video, detector, detections_path, nth_frame = 1):
    cap = cv2.VideoCapture(path_to_video)

    count_frame = 0
    detections = []
    fourcc = cv2.VideoWriter_fourcc(*"mp4v")

    # new_video = cv2.VideoWriter("visualized_video/output_detections_orange.avi", fourcc=fourcc, fps=25, apiPreference=0,
    #                             frameSize=(int(1920 / 4), int(1080 / 4)))
    all_time = 0
    while True:
        ret, frame = cap.read()

        if not ret or frame is None:
            break

        if count_frame % nth_frame == 0:
            before_time = time.time()
            frame_detections = detector.detect(frame)
            all_time += time.time() - before_time
            detections.append(frame_detections)
        else:
            detections.append([])

        count_frame += 1
        if count_frame % 10 == 0:
            print(count_frame)

        # if count_frame > 200:
        #     break

    print(count_frame/all_time)
    print(all_time)
    json_detections = {"data": detections}
    json.dump(json_detections, open(detections_path, "w"))